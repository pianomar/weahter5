package com.omarhezi.weather5.ViewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omarhezi.weather5.DTOs.TempDTO;
import com.omarhezi.weather5.DTOs.WeatherDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;
import com.omarhezi.weather5.Miscellaneous.TextHelper;
import com.omarhezi.weather5.Miscellaneous.Weather5Tools;
import com.omarhezi.weather5.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherViewHolder extends RecyclerView.ViewHolder {

    private final View mItemView;

    @BindView(R.id.txt_vw_weatherItemDate)
    TextView mTxtVwWeatherItemDate;
    @BindView(R.id.txt_vw_weatherItemStatus)
    TextView mTxtVwWeatherItemStatus;
    @BindView(R.id.img_vw_weatherItemStatus)
    ImageView mImgVwWeatherItemStatus;
    @BindView(R.id.txt_vw_weatherItemMaxTemp)
    TextView mTxtVwWeatherItemMaxTemp;
    @BindView(R.id.txt_vw_weatherItemMinTemp)
    TextView mTxtVwWeatherItemMinTemp;

    public WeatherViewHolder(@NonNull View itemView) {
        super(itemView);
        mItemView = itemView;
        ButterKnife.bind(this, mItemView);
    }

    public void onBindViewHolder(WeatherDataDTO weather) {
        WeatherDTO weatherDTO = weather.getWeatherDTO().get(0);
        TempDTO temp = weather.getTemp();

        if (weatherDTO != null) {

            mTxtVwWeatherItemDate.setText(Weather5Tools.timeStampToString(weather.getDateTimeStampSeconds() *1000));
            mTxtVwWeatherItemStatus.setText(TextHelper.capitalize(weatherDTO.getDescription()));
            Glide.with(mItemView).load(Weather5Tools.generateIconLink(weatherDTO.getIcon())).into(mImgVwWeatherItemStatus);
            mTxtVwWeatherItemMaxTemp.setText(Weather5Tools.getDegreeText(temp.getDay()));
            mTxtVwWeatherItemMinTemp.setText(Weather5Tools.getDegreeText(temp.getNight()));
        }
    }
}