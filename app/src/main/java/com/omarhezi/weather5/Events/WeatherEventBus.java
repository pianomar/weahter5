package com.omarhezi.weather5.Events;

import org.greenrobot.eventbus.EventBus;

public class WeatherEventBus implements IWeatherEventBus {

    private EventBus mEventBus;
    private static WeatherEventBus instance = new WeatherEventBus();

    private WeatherEventBus() {
        mEventBus = EventBus.getDefault();
    }

    public static WeatherEventBus getInstance() {
        return instance;
    }

    @Override
    public void register(Object subscriber) {
        if (!mEventBus.isRegistered(subscriber)) {
            mEventBus.register(subscriber);
        }
    }

    @Override
    public void unregister(Object subscriber) {
        if (mEventBus.isRegistered(subscriber)) {
            mEventBus.unregister(subscriber);
        }
    }

    @Override
    public void post(Object event) {
        mEventBus.post(event);
    }
}
