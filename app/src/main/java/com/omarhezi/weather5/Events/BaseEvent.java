package com.omarhezi.weather5.Events;

public class BaseEvent {
    private int mCode;

    public void setCode(int code) {
        mCode = code;
    }

    public int getCode() {
        return mCode;
    }
}
