package com.omarhezi.weather5.Events;

public class LocationEvent extends BaseEvent {
    public static final int LOCATION_PERMISSION_GRANTED = 1;
    public static final int LOCATION_PERMISSION_DENIED = 2;
    public static final int LOCATION_OBTAINED = 3;
    public static final int CITY_SELECTED = 4;
    public static final int CITY_CLEARED = 5;
    public static final int CITY_NAMED = 6;

    private double mLatitude;
    private double mLongitude;
    private String mCityId;
    private String mCityName;

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public void setCityId(String cityId) {
        mCityId = cityId;
    }

    public String getCityId() {
        return mCityId;
    }

    public void setCityName(String cityName) {
        mCityName = cityName;
    }

    public String getCityName() {
        return mCityName;
    }
}
