package com.omarhezi.weather5.Events;

public interface IWeatherEventBus {
    void register(Object subscriber);

    void unregister(Object subscriber);

    void post(Object event);
}
