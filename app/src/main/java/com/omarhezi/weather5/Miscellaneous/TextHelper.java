package com.omarhezi.weather5.Miscellaneous;

import androidx.annotation.NonNull;

public class TextHelper {
    public static String capitalize(@NonNull  String text) {
        return text.substring(0,1).toUpperCase() + text.substring(1);
    }
}
