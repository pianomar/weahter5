package com.omarhezi.weather5.Miscellaneous;

import android.text.format.DateFormat;

import com.omarhezi.weather5.Repositores.HomeRepository;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;

public class Weather5Tools {
    public static String formatToTwoDecimals(Float number) {
        return String.format(Locale.getDefault(), "%.0f", Math.floor(number));
    }

    public static String timeStampToString(@NonNull Long timeStamp) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timeStamp);
        return DateFormat.format(Constants.TODAY_DATE_FORMAT, cal).toString();
    }

    public static String getTodayDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.TODAY_DATE_TIME_FORMAT, Locale.getDefault());
        Date now = new Date();
        return sdf.format(now);
    }

    public static String generateIconLink(String iconName) {
        return HomeRepository.BASE_ICON_URL + iconName + ".png";
    }

    public static String getDegreeText(Float degree) {
        return formatToTwoDecimals(degree) + Constants.DEGREE_CHAR;
    }
}
