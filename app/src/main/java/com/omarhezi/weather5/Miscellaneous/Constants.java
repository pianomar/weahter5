package com.omarhezi.weather5.Miscellaneous;

public class Constants {
    //Formatting
    public static final String CELSIUS_CHAR = "\u2103";
    public static final String DEGREE_CHAR = "\u00B0";
    public static final String TODAY_DATE_TIME_FORMAT = "MMMM dd, HH:mm";
    public static final String TODAY_DATE_FORMAT = "MMMM dd";

    //Cities Ids
    public static final String MOSCOW_ID   = "524901";
    public static final String DAMASCUS_ID = "170654";
    public static final String MIAMI_ID    = "4164138";
    public static final String LONDON_ID   = "2643743";
    public static final String LIMA_ID     = "3936456";
    public static final String SELECTED_CITY_ID_KEY = "selected_city_id";
}
