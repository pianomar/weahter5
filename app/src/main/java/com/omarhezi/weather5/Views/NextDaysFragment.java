package com.omarhezi.weather5.Views;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.material.snackbar.Snackbar;
import com.omarhezi.weather5.Adapters.WeatherAdapter;
import com.omarhezi.weather5.DTOs.NextDaysDTO;
import com.omarhezi.weather5.Presenter.Interfaces.INextDaysPresenter;
import com.omarhezi.weather5.Presenter.NextDaysPresenter;
import com.omarhezi.weather5.R;
import com.omarhezi.weather5.Views.Interfaces.INextDaysView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class NextDaysFragment extends Fragment implements INextDaysView {

    public static final int INDEX = 1;
    @BindView(R.id.rec_vw_nextDays)
    RecyclerView mRecVwNextDays;
    @BindView(R.id.root_vw_nextDays)
    FrameLayout mRootVwNextDays;
    @BindView(R.id.next_days_progress)
    View mProgress;

    private Unbinder mUnbinder;

    private WeatherAdapter mAdapter;
    private INextDaysPresenter mPresenter;

    public NextDaysFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_next_days, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        setupPresenter();
        setupRecyclerView();
        return view;
    }

    private void setupPresenter() {
        mPresenter = new NextDaysPresenter(this);
        mPresenter.load();
    }

    private void setupRecyclerView() {
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecVwNextDays.setLayoutManager(layoutManager);
        mAdapter = new WeatherAdapter();
        mRecVwNextDays.setAdapter(mAdapter);
    }


    @Override
    public void onDestroy() {
        mUnbinder.unbind();
        mPresenter.destroy();
        super.onDestroy();
    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(mRootVwNextDays, message, Snackbar.LENGTH_LONG);
    }

    @Override
    public void showProgress(boolean show) {
        if (getActivity() != null && isAdded()) {
            getActivity().runOnUiThread(() -> mProgress.setVisibility(show ? View.VISIBLE : View.GONE));
        }
    }

    @Override
    public void onReceivedNextDaysForecast(NextDaysDTO nextDaysDTO) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(() -> mAdapter.setItems(nextDaysDTO.getWeatherDataList()));
        }
    }
}