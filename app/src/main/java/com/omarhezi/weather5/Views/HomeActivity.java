package com.omarhezi.weather5.Views;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.tabs.TabLayout;
import com.omarhezi.weather5.Miscellaneous.Constants;
import com.omarhezi.weather5.Presenter.HomePresenter;
import com.omarhezi.weather5.Presenter.Interfaces.IHomePresenter;
import com.omarhezi.weather5.R;
import com.omarhezi.weather5.UI.PagerAdapters.HomePagerAdapter;
import com.omarhezi.weather5.Views.Interfaces.IHomeView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements IOnFragmentReady, IHomeView {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 10;

    @BindView(R.id.tab_layout_home)
    TabLayout mTabLayoutHome;
    @BindView(R.id.vw_pager_home)
    ViewPager mVwPagerHome;

    private FusedLocationProviderClient mFusedLocationClient;
    private IHomePresenter mPresenter;
    private String mSelectedCityId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPresenter = new HomePresenter(this);
        mPresenter.load();
        setupPagerAdapter();
    }

    private void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isLocationPermissionGranted()) {
                onPermissionGranted();
            } else {
                if (shouldShowPermissionRationale()) {
                    // TODO: 3/16/2019 Show Permission rationale dialog
                } else {
                    String[] locationPermissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION};
                    requestPermissions(locationPermissions, LOCATION_PERMISSION_REQUEST_CODE);
                }
            }
        } else {
            onPermissionGranted();
        }
    }

    private void onPermissionGranted() {
        mPresenter.onLocationPermissionGranted();
        getCurrentUserLocation();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean shouldShowPermissionRationale() {
        return shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ||
                shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isLocationPermissionGranted() {
        return checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        mPresenter.onLocationPermissionGranted();
                    }
                } else {
                    mPresenter.onLocationPermissionDenied();
                }
                break;
        }
    }

    private void setupPagerAdapter() {
        HomePagerAdapter myPagerAdapter = new HomePagerAdapter(getSupportFragmentManager());
        mVwPagerHome.setAdapter(myPagerAdapter);
        mTabLayoutHome.setupWithViewPager(mVwPagerHome);
    }

    @SuppressLint("MissingPermission")
    public void getCurrentUserLocation() {
        new Thread(() -> {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
                if (location != null) {
                    mPresenter.sendLocationObtainedEvent(location.getLatitude(), location.getLongitude());
                } else {
                    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    mPresenter.getUserLocation(locationManager);
                }
            });
        }).run();
    }

    @Override
    protected void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
    }

    @Override
    public void onFragmentReady() {
        checkLocationPermission();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_filter) {
            FilterDialog dialog = new FilterDialog();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.SELECTED_CITY_ID_KEY, mSelectedCityId);
            dialog.setArguments(bundle);
            dialog.setListener(cityId -> {
                mSelectedCityId = cityId;
                mPresenter.onSelectedCityFilter(cityId);
            });
            dialog.show(transaction, FilterDialog.TAG);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setCityName(String cityName) {
        runOnUiThread(() -> {
            setTitle(cityName );
        });
    }
}
