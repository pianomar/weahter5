package com.omarhezi.weather5.Views.Interfaces;

import com.omarhezi.weather5.DTOs.WeatherDataDTO;

public interface ITodayHomeView extends IBaseView {
    void showPermissionDeniedView(boolean show);

    void populateWeatherData(WeatherDataDTO weather, String todayDate);
}
