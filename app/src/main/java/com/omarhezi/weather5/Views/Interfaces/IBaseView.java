package com.omarhezi.weather5.Views.Interfaces;

public interface IBaseView {
    void showErrorMessage(String message);

    void showProgress(boolean show);
}
