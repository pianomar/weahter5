package com.omarhezi.weather5.Views;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.omarhezi.weather5.Miscellaneous.Constants;
import com.omarhezi.weather5.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FilterDialog extends DialogFragment {

    static final String TAG = "FilterDialog";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.radio_btn_london)
    RadioButton mRadioBtnLondon;
    @BindView(R.id.radio_btn_damascus)
    RadioButton mRadioBtnDamascus;
    @BindView(R.id.radio_btn_lima)
    RadioButton mRadioBtnLima;
    @BindView(R.id.radio_btn_miami)
    RadioButton mRadioBtnMiami;
    @BindView(R.id.radio_btn_moscow)
    RadioButton mRadioBtnMoscow;
    @BindView(R.id.radio_group_cities)
    RadioGroup mRadioGroupCities;
    @BindView(R.id.btn_apply_filter)
    Button mBtnApplyFilter;
    @BindView(R.id.btn_clear_filter)
    Button mBtnClearFilters;

    private Unbinder mUnbiner;
    private IFilterListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_cities_filter, container, false);
        mUnbiner = ButterKnife.bind(this, view);
        setupToolbar(mToolbar);
        mBtnApplyFilter.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClickApplyFilter(getCheckedCityId());
                dismiss();
            }
        });

        if (getArguments() != null) {
            RadioButton selectedCity = (RadioButton) mRadioGroupCities.findViewWithTag(getArguments().getString(Constants.SELECTED_CITY_ID_KEY));
            if (selectedCity != null) {
                selectedCity.setChecked(true);
            }
        }

        mBtnClearFilters.setOnClickListener(v -> mRadioGroupCities.clearCheck());
        return view;
    }

    private String getCheckedCityId() {
        String selectedTagId = null;
        Object selectedTag = null;

        if (mRadioGroupCities.getCheckedRadioButtonId() != 0) {
            View radioButton = mRadioGroupCities.findViewById(mRadioGroupCities.getCheckedRadioButtonId());

            if (radioButton != null) {
                selectedTag = radioButton.getTag();
            }

            if (selectedTag != null) {
                selectedTagId = selectedTag.toString();
            }
        }

        return selectedTagId;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    private void setupToolbar(Toolbar toolbar) {
        toolbar.setNavigationIcon(R.drawable.close_white);
        toolbar.setNavigationOnClickListener(view -> dismiss());
        toolbar.setTitle(getString(R.string.lbl_filter));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnbiner != null) {
            mUnbiner.unbind();
        }
    }

    public void setListener(IFilterListener listener) {
        mListener = listener;
    }

    public interface IFilterListener {
        void onClickApplyFilter(String cityId);
    }
}
