package com.omarhezi.weather5.Views;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.omarhezi.weather5.DTOs.MainDTO;
import com.omarhezi.weather5.DTOs.WeatherDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;
import com.omarhezi.weather5.Miscellaneous.Weather5Tools;
import com.omarhezi.weather5.Presenter.Interfaces.ITodayHomePresenter;
import com.omarhezi.weather5.Presenter.TodayHomePresenter;
import com.omarhezi.weather5.R;
import com.omarhezi.weather5.Views.Interfaces.ITodayHomeView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

interface IOnFragmentReady {
    void onFragmentReady();
}

public class TodayHomeFragment extends Fragment implements ITodayHomeView {

    public static final int INDEX = 0;

    @BindView(R.id.img_vw_noLocation)
    ImageView mImgVwNoLocation;
    @BindView(R.id.const_vw_noLocationView)
    ConstraintLayout mConstVwNoLocationView;
    @BindView(R.id.txt_vw_dateTime)
    TextView mTxtVwDateTime;
    @BindView(R.id.txt_vw_minMaxTemp)
    TextView mTxtVwMinMaxTemp;
    @BindView(R.id.txt_vw_mainTemp)
    TextView mTxtVwMainTemp;
    @BindView(R.id.txt_vw_description)
    TextView mTxtVwDescription;
    @BindView(R.id.img_vw_status)
    ImageView mImgVwStatus;
    @BindView(R.id.root_vw_today)
    View mRootVw;
    @BindView(R.id.today_progress)
    View mProgress;

    private ITodayHomePresenter mPresenter;
    private IOnFragmentReady mListener;
    private Unbinder mUnbinder;

    public TodayHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDestroy() {
        mPresenter.destroy();
        super.onDestroy();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IOnFragmentReady) {
            mListener = (IOnFragmentReady) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today_home, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mPresenter = new TodayHomePresenter(this);
        mPresenter.load();
        if (mListener != null) {
            mListener.onFragmentReady();
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void showPermissionDeniedView(boolean show) {
        mConstVwNoLocationView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void populateWeatherData(WeatherDataDTO weather, String todayDate) {
        if (getActivity() != null && isAdded()) {
            getActivity().runOnUiThread(() -> {
                MainDTO mainDTO = weather.getMainDTO();
                List<WeatherDTO> weatherDTOs = weather.getWeatherDTO();

                mTxtVwDateTime.setText(todayDate);
                mTxtVwMainTemp.setText(mainDTO.getTempString());
                if (weatherDTOs.get(0) != null) {
                    mTxtVwDescription.setText(weatherDTOs.get(0).getMain());
                }
                setMinMaxTemperature(mainDTO);
                setWeatherIcon(weatherDTOs);
                mPresenter.setCityName(weather.getCityName());
            });
        }
    }

    private void setMinMaxTemperature(MainDTO mainDTO) {
        mTxtVwMinMaxTemp.setText(String.format("%s%s ● %s%s",
                mainDTO.getTempMinString(), getString(R.string.lbl_min),
                mainDTO.getTempMaxString(), getString(R.string.lbl_max)));
    }

    private void setWeatherIcon(List<WeatherDTO> weatherDTO) {
        if (weatherDTO.get(0) != null) {
            Glide.with(this)
                    .load(Weather5Tools.generateIconLink(weatherDTO.get(0).getIcon()))
                    .into(mImgVwStatus);
        }
    }

    @Override
    public void showErrorMessage(String message) {
        Snackbar.make(mRootVw, message, Snackbar.LENGTH_LONG);
    }

    @Override
    public void showProgress(boolean show) {
        if (getActivity() != null && isAdded()) {
            getActivity().runOnUiThread(() -> mProgress.setVisibility(show ? View.VISIBLE : View.GONE));
        }
    }
}
