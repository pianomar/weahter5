package com.omarhezi.weather5.Views.Interfaces;

import com.omarhezi.weather5.DTOs.NextDaysDTO;

public interface INextDaysView extends IBaseView {
    void onReceivedNextDaysForecast(NextDaysDTO nextDaysDTO);
}
