package com.omarhezi.weather5.DTOs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NextDaysDTO extends BaseDTO {
    @SerializedName("list")
    private List<WeatherDataDTO> mWeatherDataList;

    public List<WeatherDataDTO> getWeatherDataList() {
        return mWeatherDataList;
    }

    public void setWeatherDataList(List<WeatherDataDTO> weatherDataList) {
        mWeatherDataList = weatherDataList;
    }
}
