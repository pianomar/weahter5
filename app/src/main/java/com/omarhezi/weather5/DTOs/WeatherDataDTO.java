package com.omarhezi.weather5.DTOs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherDataDTO extends BaseDTO {
    @SerializedName("dt")
    private Long mDateTimeStamp;
    @SerializedName("main")
    private MainDTO mMainDTO;
    @SerializedName("weather")
    private List<WeatherDTO> mWeatherDTO;
    @SerializedName("name")
    private String mCityName;
    @SerializedName("temp")
    private TempDTO mTemp;

    public MainDTO getMainDTO() {
        return mMainDTO;
    }

    public List<WeatherDTO> getWeatherDTO() {
        return mWeatherDTO;
    }

    public String getCityName() {
        return mCityName;
    }

    public Long getDateTimeStampSeconds() {
        return mDateTimeStamp;
    }

    public TempDTO getTemp() {
        return mTemp;
    }
}
