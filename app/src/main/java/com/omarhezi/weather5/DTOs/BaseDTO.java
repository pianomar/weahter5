package com.omarhezi.weather5.DTOs;

public class BaseDTO {
    private int mType;

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }
}
