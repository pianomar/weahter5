package com.omarhezi.weather5.DTOs;

import com.google.gson.annotations.SerializedName;

public class TempDTO {
    @SerializedName("day")
    private Float day;
    @SerializedName("min")
    private Float min;
    @SerializedName("max")
    private Float max;
    @SerializedName("night")
    private Float night;
    @SerializedName("eve")
    private Float eve;
    @SerializedName("morn")
    private Float morn;

    public Float getDay() {
        return day;
    }

    public void setDay(Float day) {
        this.day = day;
    }

    public Float getMin() {
        return min;
    }

    public void setMin(Float min) {
        this.min = min;
    }

    public Float getMax() {
        return max;
    }

    public void setMax(Float max) {
        this.max = max;
    }

    public Float getNight() {
        return night;
    }

    public void setNight(Float night) {
        this.night = night;
    }

    public Float getEve() {
        return eve;
    }

    public void setEve(Float eve) {
        this.eve = eve;
    }

    public Float getMorn() {
        return morn;
    }

    public void setMorn(Float morn) {
        this.morn = morn;
    }
}
