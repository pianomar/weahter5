package com.omarhezi.weather5.DTOs;

import com.google.gson.annotations.SerializedName;
import com.omarhezi.weather5.Miscellaneous.Constants;
import com.omarhezi.weather5.Miscellaneous.Weather5Tools;

public class MainDTO {

    @SerializedName("temp")
    private Float mTemp;
    @SerializedName("pressure")
    private Float mPressure;
    @SerializedName("humidity")
    private Integer mHumidity;
    @SerializedName("temp_min")
    private Float mTempMin;
    @SerializedName("temp_max")
    private Float mTempMax;

    private String mTempString;

    public Float getTemp() {
        return mTemp;
    }

    public void setTemp(Float temp) {
        mTemp = temp;
    }

    public Float getPressure() {
        return mPressure;
    }

    public void setPressure(Float pressure) {
        mPressure = pressure;
    }

    public Integer getHumidity() {
        return mHumidity;
    }

    public void setHumidity(Integer humidity) {
        mHumidity = humidity;
    }

    public Float getTempMin() {
        return mTempMin;
    }

    public void setTempMin(Float tempMin) {
        mTempMin = tempMin;
    }

    public Float getTempMax() {
        return mTempMax;
    }

    public void setTempMax(Float tempMax) {
        mTempMax = tempMax;
    }

    public void setTempString(String tempString) {
        mTempString = tempString;
    }

    public String getTempString() {
        return mTempString;
    }

    public String getTempMaxString() {
        return Weather5Tools.getDegreeText(getTempMax());
    }

    public String getTempMinString() {
        return Weather5Tools.getDegreeText(getTempMin());
    }
}
