package com.omarhezi.weather5.Repositores;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.omarhezi.weather5.BuildConfig;
import com.omarhezi.weather5.DTOs.NextDaysDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;
import com.omarhezi.weather5.Repositores.Interfaces.IHomeRepository;
import com.omarhezi.weather5.Repositores.Interfaces.WeatherAPI;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeRepository implements IHomeRepository {
    private static final String APPID_KEY = "appid";
    public static final String BASE_ICON_URL = "http://openweathermap.org/img/w/";
    private static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";

    @Override
    public Observable<WeatherDataDTO> getWeatherByCoordinates(Map<String, String> queryMap) {
        return getRetrofit().create(WeatherAPI.class)
                .getCurrentWeatherDataByCoordinates(queryMap)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread());
    }

    @Override
    public Observable<NextDaysDTO> getNextDaysWeatherWithCoordinates(Map<String, String> queryMap) {
        return getRetrofit().create(WeatherAPI.class)
                .getNextDaysWeather(queryMap)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread());
    }

    private Retrofit getRetrofit() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient client = buildInterceptor();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    private OkHttpClient buildInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();

        client.addInterceptor(chain -> {
            Request request = chain.request();
            HttpUrl url = request.url().newBuilder().addQueryParameter(APPID_KEY, BuildConfig.OpenWeatherMapAPI).build();
            request = request.newBuilder().url(url).build();
            return chain.proceed(request);
        });

        client.addInterceptor(interceptor);
        return client.build();
    }
}
