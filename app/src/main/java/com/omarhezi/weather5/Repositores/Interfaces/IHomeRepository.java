package com.omarhezi.weather5.Repositores.Interfaces;

import com.omarhezi.weather5.DTOs.NextDaysDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;

import java.util.Map;

import io.reactivex.Observable;

public interface IHomeRepository {
    Observable<WeatherDataDTO> getWeatherByCoordinates(Map<String, String> queryMap);

    Observable<NextDaysDTO> getNextDaysWeatherWithCoordinates(Map<String, String> map);
}
