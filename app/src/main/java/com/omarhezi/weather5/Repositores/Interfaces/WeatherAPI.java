package com.omarhezi.weather5.Repositores.Interfaces;

import com.omarhezi.weather5.DTOs.NextDaysDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface WeatherAPI {
    @GET("weather")
    Observable<WeatherDataDTO> getCurrentWeatherDataByCoordinates
            (@QueryMap Map<String, String> queryMap);

    @GET("forecast/daily")
    Observable<NextDaysDTO> getNextDaysWeather(@QueryMap Map<String, String> queryMap);
}
