package com.omarhezi.weather5.UseCases;

import android.annotation.SuppressLint;

import com.omarhezi.weather5.DTOs.WeatherDataDTO;
import com.omarhezi.weather5.Repositores.HomeRepository;
import com.omarhezi.weather5.Repositores.Interfaces.IHomeRepository;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class GetCurrentWeatherUseCase {

    private IGetCurrentWeatherUserCase mListener;
    private String mUnits = "metric";
    private boolean mIsCurrentLocation;
    private WeatherDataDTO mCurrentLocationWeather;

    public interface IGetCurrentWeatherUserCase {
        void onReceivedWeatherData(WeatherDataDTO weather);

        void onError(String message);
    }

    public void executeWithCityId(String cityId) {
        Map<String, String> map = new HashMap<>();
        map.put("id", cityId);
        map.put("units", mUnits);
        mIsCurrentLocation = false;
        execute(map);
    }

    public void executeWithCoordinates(Double latitude, Double longitude) {
        Map<String, String> map = setupQueryParams(latitude, longitude);
        mIsCurrentLocation = true;
        execute(map);
    }

    @SuppressLint("CheckResult")
    private void execute(Map<String, String> map) {
        IHomeRepository repository = new HomeRepository();
        repository.getWeatherByCoordinates(map).subscribeWith(new Observer<WeatherDataDTO>() {
            @Override
            public void onSubscribe(Disposable d) {
                //Nothing here
            }

            @Override
            public void onNext(WeatherDataDTO weatherDataDTO) {
                if (mIsCurrentLocation) {
                    mCurrentLocationWeather = weatherDataDTO;
                }

                if (mListener != null) {
                    mListener.onReceivedWeatherData(weatherDataDTO);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (mListener != null) {
                    if (e != null) {
                        mListener.onError(e.getMessage());
                    }
                }
            }

            @Override
            public void onComplete() {
                //Nothing here
            }
        });
    }

    private Map<String, String> setupQueryParams(Double latitude, Double longitude) {
        Map<String, String> map = new HashMap<>();
        map.put("lon", longitude.toString());
        map.put("lat", latitude.toString());
        map.put("units", mUnits);
        return map;
    }

    public void setListener(IGetCurrentWeatherUserCase listener) {
        mListener = listener;
    }

    public WeatherDataDTO getCurrentLocationWeather() {
        return mCurrentLocationWeather;
    }

    public void setUnits(String units) {
        mUnits = units;
    }
}
