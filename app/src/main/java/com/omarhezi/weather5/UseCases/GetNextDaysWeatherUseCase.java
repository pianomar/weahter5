package com.omarhezi.weather5.UseCases;

import android.annotation.SuppressLint;

import com.omarhezi.weather5.DTOs.NextDaysDTO;
import com.omarhezi.weather5.Repositores.HomeRepository;
import com.omarhezi.weather5.Repositores.Interfaces.IHomeRepository;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class GetNextDaysWeatherUseCase {

    private IOnGetNextDaysListener mListener;
    private String mUnits = "metric";
    private boolean mIsCurrentLocation;
    private NextDaysDTO mCurrentLocationWeather;

    public void executeWithCityId(int numDays, String cityId) {
        Map<String, String> map = new HashMap<>();
        map.put("cnt", String.valueOf(numDays));
        map.put("id", cityId);
        map.put("units", mUnits);
        mIsCurrentLocation = false;
        execute(map);
    }

    public interface IOnGetNextDaysListener {
        void onGetNextDaysForecast(NextDaysDTO nextDaysDTO);

        void onError(String message);
    }

    public void executeWithCoordinates(int numDays, Double latitude, Double longitude) {
        Map<String, String> map = setupQueryParams(numDays, latitude, longitude);
        mIsCurrentLocation = true;
        execute(map);
    }

    @SuppressLint("CheckResult")
    private void execute(Map<String, String> map) {
        IHomeRepository repository = new HomeRepository();
        repository.getNextDaysWeatherWithCoordinates(map).subscribeWith(new Observer<NextDaysDTO>() {
            @Override
            public void onSubscribe(Disposable d) {
                //Nothing here
            }

            @Override
            public void onNext(NextDaysDTO nextDaysDTO) {
                if (mIsCurrentLocation) {
                    mCurrentLocationWeather = nextDaysDTO;
                }

                if (mListener != null) {
                    mListener.onGetNextDaysForecast(nextDaysDTO);
                }
            }

            @Override
            public void onError(Throwable e) {
                if (mListener != null) {
                    mListener.onError(e.getMessage());
                }
            }

            @Override
            public void onComplete() {
                //Nothing here
            }
        });
    }

    private Map<String, String> setupQueryParams(int numDays, Double latitude, Double longitude) {
        Map<String, String> map = new HashMap<>();
        map.put("lon", longitude.toString());
        map.put("lat", latitude.toString());
        map.put("cnt", String.valueOf(numDays));
        map.put("units", mUnits);
        return map;
    }

    public void setListener(IOnGetNextDaysListener listener) {
        mListener = listener;
    }

    public NextDaysDTO getCurrentLocationWeather() {
        return mCurrentLocationWeather;
    }
}
