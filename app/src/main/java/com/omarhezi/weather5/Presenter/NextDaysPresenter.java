package com.omarhezi.weather5.Presenter;

import com.omarhezi.weather5.Adapters.WeatherAdapterDelegate;
import com.omarhezi.weather5.DTOs.NextDaysDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;
import com.omarhezi.weather5.Events.LocationEvent;
import com.omarhezi.weather5.Events.WeatherEventBus;
import com.omarhezi.weather5.Presenter.Interfaces.INextDaysPresenter;
import com.omarhezi.weather5.UseCases.GetNextDaysWeatherUseCase;
import com.omarhezi.weather5.Views.Interfaces.INextDaysView;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class NextDaysPresenter implements INextDaysPresenter {

    private static final int DAYS_LIMIT = 5;

    private INextDaysView mView;
    private GetNextDaysWeatherUseCase mUseCase;

    public NextDaysPresenter(INextDaysView view) {
        mView = view;
    }

    @Override
    public void load() {
        WeatherEventBus.getInstance().register(this);
    }

    @Override
    public void destroy() {
        mView = null;
        WeatherEventBus.getInstance().unregister(this);
    }

    @Override
    public void getNexDaysWeather(double latitude, double longitude) {
        mUseCase = new GetNextDaysWeatherUseCase();
        getWeatherRequestListener();
        mUseCase.executeWithCoordinates(5, latitude, longitude);
    }

    private void setupDaysForecastList(NextDaysDTO nextDaysDTO) {
        List<WeatherDataDTO> weatherDataList = nextDaysDTO.getWeatherDataList();
        for (WeatherDataDTO weatherDataDTO : weatherDataList) {
            weatherDataDTO.setType(WeatherAdapterDelegate.DELEGATE_TYPE);
        }
    }

    @Subscribe
    public void onLocationEvent(LocationEvent event) {
        switch (event.getCode()) {
            case LocationEvent.LOCATION_OBTAINED:
                getNexDaysWeather(event.getLatitude(), event.getLongitude());
                break;
            case LocationEvent.CITY_SELECTED:
                getNexDaysWeatherByCityId(event.getCityId());
                break;
            case LocationEvent.CITY_CLEARED:
                resetData();
                break;
        }
    }

    private void resetData() {
        if (mView != null) {
            mView.onReceivedNextDaysForecast(mUseCase.getCurrentLocationWeather());
        }
    }

    private void getNexDaysWeatherByCityId(String cityId) {
        if (mView != null) {
            mView.showProgress(true);
        }
        mUseCase.executeWithCityId(DAYS_LIMIT, cityId);
    }

    private void getWeatherRequestListener() {
        mUseCase.setListener(new GetNextDaysWeatherUseCase.IOnGetNextDaysListener() {
            @Override
            public void onGetNextDaysForecast(NextDaysDTO nextDaysDTO) {
                if (mView != null) {
                    setupDaysForecastList(nextDaysDTO);
                    mView.onReceivedNextDaysForecast(nextDaysDTO);
                    mView.showProgress(false);
                }
            }

            @Override
            public void onError(String message) {
                if (mView != null) {
                    mView.showErrorMessage(message);
                    mView.showProgress(false);
                }
            }
        });
    }
}
