package com.omarhezi.weather5.Presenter.Interfaces;

public interface IBasePresenter {
    void load();
    void destroy();
}
