package com.omarhezi.weather5.Presenter.Interfaces;

public interface ITodayHomePresenter extends IBasePresenter {
    void setCityName(String cityName);
}
