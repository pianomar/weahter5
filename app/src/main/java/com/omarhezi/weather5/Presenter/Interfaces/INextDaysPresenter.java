package com.omarhezi.weather5.Presenter.Interfaces;

public interface INextDaysPresenter extends IBasePresenter {
    void getNexDaysWeather(double latitude, double longitude);
}
