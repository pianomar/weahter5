package com.omarhezi.weather5.Presenter.Interfaces;

import android.location.LocationManager;

public interface IHomePresenter extends IBasePresenter {
    void onLocationPermissionGranted();

    void onLocationPermissionDenied();

    void sendLocationObtainedEvent(double latitude, double longitude);

    void getUserLocation(LocationManager locationManager);

    void onSelectedCityFilter(String cityId);
}
