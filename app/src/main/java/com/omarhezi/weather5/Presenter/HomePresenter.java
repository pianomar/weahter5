package com.omarhezi.weather5.Presenter;

import android.annotation.SuppressLint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.omarhezi.weather5.Events.LocationEvent;
import com.omarhezi.weather5.Events.WeatherEventBus;
import com.omarhezi.weather5.Presenter.Interfaces.IHomePresenter;
import com.omarhezi.weather5.Views.Interfaces.IHomeView;

import org.greenrobot.eventbus.Subscribe;

public class HomePresenter implements IHomePresenter {

    IHomeView mView;

    public HomePresenter(IHomeView view) {
        mView = view;
    }

    @Override
    public void onLocationPermissionGranted() {
        sendEvent(null, LocationEvent.LOCATION_PERMISSION_GRANTED);
    }

    @Override
    public void onLocationPermissionDenied() {
        sendEvent(null, LocationEvent.LOCATION_PERMISSION_DENIED);
    }

    @Override
    public void sendLocationObtainedEvent(double latitude, double longitude) {
        LocationEvent locationEvent = new LocationEvent();
        locationEvent.setLatitude(latitude);
        locationEvent.setLongitude(longitude);
        sendEvent(locationEvent, LocationEvent.LOCATION_OBTAINED);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void getUserLocation(LocationManager locationManager) {
        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    sendLocationObtainedEvent(location.getLatitude(), location.getLongitude());
                }
                locationManager.removeUpdates(this);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                //Nothing here
            }

            @Override
            public void onProviderEnabled(String provider) {
                //Nothing here
            }

            @Override
            public void onProviderDisabled(String provider) {
                //Nothing here
            }
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    @Override
    public void onSelectedCityFilter(String cityId) {
        LocationEvent locationEvent = new LocationEvent();
        if (cityId != null) {
            locationEvent.setCityId(cityId);
            sendEvent(locationEvent, LocationEvent.CITY_SELECTED);
        } else {
            sendEvent(null, LocationEvent.CITY_CLEARED);
        }
    }

    private void sendEvent(LocationEvent locationEvent, int locationPermissionDenied) {
        if (locationEvent == null) {
            locationEvent = new LocationEvent();
        }
        locationEvent.setCode(locationPermissionDenied);
        WeatherEventBus.getInstance().post(locationEvent);
    }

    @Override
    public void load() {
        WeatherEventBus.getInstance().register(this);
    }

    @Override
    public void destroy() {
        WeatherEventBus.getInstance().unregister(this);
        mView = null;
    }

    @Subscribe
    public void onLocationEvent(LocationEvent event) {
        if (event.getCode() == LocationEvent.CITY_NAMED) {
            mView.setCityName(event.getCityName());
        }
    }
}
