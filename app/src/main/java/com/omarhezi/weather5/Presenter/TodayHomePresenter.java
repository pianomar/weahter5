package com.omarhezi.weather5.Presenter;

import com.omarhezi.weather5.DTOs.MainDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;
import com.omarhezi.weather5.Events.LocationEvent;
import com.omarhezi.weather5.Events.WeatherEventBus;
import com.omarhezi.weather5.Miscellaneous.Constants;
import com.omarhezi.weather5.Miscellaneous.Weather5Tools;
import com.omarhezi.weather5.Presenter.Interfaces.ITodayHomePresenter;
import com.omarhezi.weather5.UseCases.GetCurrentWeatherUseCase;
import com.omarhezi.weather5.Views.Interfaces.ITodayHomeView;

import org.greenrobot.eventbus.Subscribe;

public class TodayHomePresenter implements ITodayHomePresenter {

    private ITodayHomeView mView;
    private GetCurrentWeatherUseCase mGetCurrentWeatherUseCase;

    public TodayHomePresenter(ITodayHomeView view) {
        mView = view;
    }

    private void getWeatherByCoordinates(double latitude, double longitude) {
        if (mView != null) {
            mView.showProgress(true);
        }
        mGetCurrentWeatherUseCase = new GetCurrentWeatherUseCase();
        setWeatherRequestListener();
        mGetCurrentWeatherUseCase.executeWithCoordinates(latitude, longitude);
    }

    private void resetData() {
        if (mView != null) {
            mView.populateWeatherData(mGetCurrentWeatherUseCase.getCurrentLocationWeather(), Weather5Tools.getTodayDate());
        }
    }

    private void setWeatherRequestListener() {
        mGetCurrentWeatherUseCase.setListener(new GetCurrentWeatherUseCase.IGetCurrentWeatherUserCase() {
            @Override
            public void onReceivedWeatherData(WeatherDataDTO weather) {
                if (mView != null) {
                    MainDTO mainDTO = weather.getMainDTO();
                    if (mainDTO != null) {
                        //Format temperature text with degree
                        String todayDate = Weather5Tools.getTodayDate();
                        mainDTO.setTempString(Weather5Tools.formatToTwoDecimals(mainDTO.getTemp()) + Constants.CELSIUS_CHAR);
                        mView.populateWeatherData(weather, todayDate);
                    }
                    mView.showProgress(false);
                }
            }

            @Override
            public void onError(String message) {
                if (mView != null) {
                    mView.showErrorMessage(message);
                    mView.showProgress(false);
                }
            }
        });
    }

    @Override
    public void load() {
        WeatherEventBus.getInstance().register(this);
    }

    @Override
    public void destroy() {
        WeatherEventBus.getInstance().unregister(this);
        mView = null;
    }

    @Subscribe()
    public void onPermissionEvent(LocationEvent event) {
        switch (event.getCode()) {
            case LocationEvent.LOCATION_PERMISSION_DENIED:
                showPermissionDeniedView();
                break;
            case LocationEvent.LOCATION_PERMISSION_GRANTED:
                if (mView != null) {
                    mView.showPermissionDeniedView(false);
                }
                break;
            case LocationEvent.LOCATION_OBTAINED:
                getWeatherByCoordinates(event.getLatitude(), event.getLongitude());
                break;
            case LocationEvent.CITY_SELECTED:
                getWeatherByCityId(event.getCityId());
                break;
            case LocationEvent.CITY_CLEARED:
                resetData();
                break;
        }
    }

    private void getWeatherByCityId(String cityId) {
        if (mView != null) {
            mView.showProgress(true);
        }
        mGetCurrentWeatherUseCase.executeWithCityId(cityId);
    }

    private void showPermissionDeniedView() {
        if (mView != null) {
            mView.showPermissionDeniedView(true);
        }
    }

    @Override
    public void setCityName(String cityName) {
        if (cityName != null && !cityName.isEmpty()) {
            LocationEvent event = new LocationEvent();
            event.setCityName(cityName);
            event.setCode(LocationEvent.CITY_NAMED);
            WeatherEventBus.getInstance().post(event);
        }
    }
}
