package com.omarhezi.weather5.Adapters;

import android.view.ViewGroup;

import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager;
import com.omarhezi.weather5.DTOs.WeatherDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class WeatherAdapter extends RecyclerView.Adapter {

    private AdapterDelegatesManager<List<WeatherDataDTO>> mDelegatesManager;
    private List<WeatherDataDTO> mItems;

    public WeatherAdapter() {
        mDelegatesManager = new AdapterDelegatesManager<>();
        // AdapterDelegatesManager internally assigns view types integers
        mDelegatesManager.addDelegate(new WeatherAdapterDelegate());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return mDelegatesManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        mDelegatesManager.onBindViewHolder(mItems, position, holder);
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public void setItems(List<WeatherDataDTO> items) {
        mItems = items;
        notifyDataSetChanged();
    }
}
