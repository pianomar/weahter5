package com.omarhezi.weather5.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate;
import com.omarhezi.weather5.DTOs.WeatherDTO;
import com.omarhezi.weather5.DTOs.WeatherDataDTO;
import com.omarhezi.weather5.R;
import com.omarhezi.weather5.ViewHolders.WeatherViewHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class WeatherAdapterDelegate extends AdapterDelegate<List<WeatherDataDTO>> {

    public static final int DELEGATE_TYPE = 10;

    @Override
    protected boolean isForViewType(@NonNull List<WeatherDataDTO> items, int position) {
        return items.get(position) != null && items.get(position).getType() == DELEGATE_TYPE;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_list_item, parent, false);
        return new WeatherViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull List<WeatherDataDTO> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        WeatherViewHolder viewHolder = (WeatherViewHolder) holder;
        WeatherDataDTO weather = items.get(position);
        viewHolder.onBindViewHolder(weather);
    }
}
