package com.omarhezi.weather5.UI.PagerAdapters;

import com.omarhezi.weather5.Views.NextDaysFragment;
import com.omarhezi.weather5.Views.TodayHomeFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class HomePagerAdapter extends FragmentPagerAdapter {

    private static final int HOME_FRAGMENTS_NUM = 2;

    public HomePagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case TodayHomeFragment.INDEX:
                return new TodayHomeFragment();
            case NextDaysFragment.INDEX:
                return new NextDaysFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return HOME_FRAGMENTS_NUM;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case TodayHomeFragment.INDEX:
                return "today";
            case NextDaysFragment.INDEX:
                return "5 days";
        }
        return null;
    }
}
